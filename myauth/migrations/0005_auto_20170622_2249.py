# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myauth', '0004_templink'),
    ]

    operations = [
        migrations.AlterField(
            model_name='templink',
            name='link',
            field=models.SlugField(),
        ),
    ]
