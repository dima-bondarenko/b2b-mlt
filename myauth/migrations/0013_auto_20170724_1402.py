# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myauth', '0012_auto_20170723_2358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='templink',
            name='timeout',
            field=models.DateTimeField(default=datetime.date(2017, 7, 26)),
        ),
    ]
