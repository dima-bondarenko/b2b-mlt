# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myauth', '0010_auto_20170630_0225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='templink',
            name='timeout',
            field=models.DateTimeField(default=datetime.date(2017, 7, 14)),
        ),
    ]
