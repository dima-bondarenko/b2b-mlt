# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myauth', '0005_auto_20170622_2249'),
    ]

    operations = [
        migrations.AddField(
            model_name='templink',
            name='timeout',
            field=models.DateTimeField(default=datetime.date(2017, 6, 25)),
        ),
    ]
