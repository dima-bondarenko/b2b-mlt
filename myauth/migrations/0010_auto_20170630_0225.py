# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('myauth', '0009_auto_20170629_1652'),
    ]

    operations = [
        migrations.AlterField(
            model_name='templink',
            name='timeout',
            field=models.DateTimeField(default=datetime.date(2017, 7, 2)),
        ),
    ]
