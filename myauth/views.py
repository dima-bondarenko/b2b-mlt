import hashlib
from random import random

from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.utils.translation import ugettext as _

from .forms import AuthorizationForm, RegistrationForm, UserEditForm, \
    ChangePasswordForm
from .models import TempLink


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    if request.method == 'GET':
        return render(request, 'myauth/auth.html', {
            'heading': _('Login'),
            'submit_text': _('Login'),
            'form': AuthorizationForm(),
        })
    elif request.method == 'POST':
        form = AuthorizationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = auth.authenticate(username=username, password=password)
            if user is not None and user.is_active:
                auth.login(request, user)
                return HttpResponseRedirect('/')

        return render(request, 'myauth/auth.html', {
            'heading': _('Login'),
            'submit_text': _('Login'),
            'form': form,
        })


def signup(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    if request.method == 'GET':
        return render(request, 'myauth/auth.html', {
            'heading': _('Registration'),
            'submit_text': _('Signup'),
            'form': RegistrationForm(),
        })
    elif request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = User.objects.create_user(username=username,
                                            email=email,
                                            password=password)
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')
            user.is_active = False
            user.save()

            slug = hashlib.sha1()
            slug.update(username.encode() + email.encode() +
                        str(random()).encode())
            slug = slug.hexdigest()
            TempLink.objects.create(user=user, link=slug)
            host = str(request.get_host())
            path = str(reverse_lazy('accounts:confirm', kwargs={'link': slug}))
            link = host + path
            msg = _(
                'Thanks for signing up, {name}. To activate you account, '
                'follow this link {link}',
            )
            send_mail(
                subject=_('Confirmation of registration'),
                message=msg.format(name=user.get_full_name(), link=link),
                from_email='admin@b2b.mlt.gov.ua',
                recipient_list=[email],
            )
            return HttpResponseRedirect('/')
        else:
            return render(request, 'myauth/auth.html', {
                'heading': _('Registration'),
                'submit_text': _('Signup'),
                'form': form,
            })


@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')


def confirm(request, link):
    link = get_object_or_404(TempLink, link=link)
    if link.timeout >= timezone.now():
        user = link.user
        link.delete()
        user.is_active = True
        user.save()
        return HttpResponseRedirect(reverse_lazy('accounts:login'))
    else:
        raise Http404('Confirmation timeout')


@login_required
def user_edit(request):
    if request.method == 'GET':
        user_form = UserEditForm(
            {
                'first_name': request.user.first_name,
                'last_name': request.user.last_name,
            },
            prefix='user',
        )
        print(request.user.first_name)
        print(request.user.last_name)
        ch_pass_form = ChangePasswordForm(prefix='pass')
        return render(request, 'myauth/auth.html', {
            'form': user_form,
            'ch_pass_form': ch_pass_form,
            'submit_text': _('Confirm'),
        })
    elif request.method == 'POST':
        form = UserEditForm(request.POST, prefix='user')
        ch_pass_form = ChangePasswordForm(request.POST, prefix='pass')
        if form.is_valid() and ch_pass_form.is_valid():
            print('forms is valid')
            print(form.cleaned_data.get('first_name'))
            print(form.cleaned_data.get('last_name'))
            request.user.first_name = form.cleaned_data.get('first_name')
            request.user.last_name = form.cleaned_data.get('last_name')
            request.user.save()

            print(ch_pass_form.is_empty)
            if not ch_pass_form.is_empty:
                new_password = ch_pass_form.cleaned_data.get('new_password')
                print(new_password)
                request.user.set_password(new_password)

            return HttpResponseRedirect('/')
        else:
            return render(request, 'myauth/auth.html', {
                'form': form,
                'ch_pass_form': ch_pass_form,
                'submit_text': _('Confirm'),
            })
