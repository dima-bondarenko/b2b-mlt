import re

from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

attrs = {'class': 'form-control',
         'data-toggle': 'tooltip',
         'data-placement': 'left',
         'required': 'required'}

reg_titles = {
    'username': _('Enter the user name. 30 or fewer characters. Use only '
                  'English letters, numbers, and this symbols: @ . + - _'),
    'first_name': _('Enter your name'),
    'last_name': _('Enter your last name'),
    'email': _('Enter your real email address. A mail must come to you to '
               'continue registration.'),
    'password': _('Create a password for your account'),
    'confirm_password': _('Confirm your password'),
}
auth_titles = {
    'username': _('Enter your username'),
    'password': _('Enter your password'),
}


def dict_updater(source_dict, value):
    result_dict = source_dict.copy()
    result_dict.update({'title': value})
    return result_dict


class RegistrationForm(forms.Form):
    username = forms.CharField(
        label=_('Username'),
        max_length=30,
        widget=forms.TextInput(
            attrs=dict_updater(attrs, reg_titles['username']),
        ),
    )
    first_name = forms.CharField(
        label=_('First name'),
        max_length=30,
        widget=forms.TextInput(
            attrs=dict_updater(attrs, reg_titles['first_name']),
        ),
    )
    last_name = forms.CharField(
        label=_('Last name'),
        max_length=30,
        widget=forms.TextInput(
            attrs=dict_updater(attrs, reg_titles['last_name']),
        ),
    )
    email = forms.EmailField(
        label=_('Email'),
        widget=forms.TextInput(
            attrs=dict_updater(attrs, reg_titles['email']),
        ),
    )
    password = forms.CharField(
        label=_('Password'),
        widget=forms.PasswordInput(
            attrs=dict_updater(attrs, reg_titles['password']),
        ),
    )
    confirm_password = forms.CharField(
        label=_('Confirm password'),
        widget=forms.PasswordInput(
            attrs=dict_updater(attrs, reg_titles['confirm_password']),
        ),
    )

    def clean_username(self):
        username = self.cleaned_data['username']
        pattern = r'^[a-zA-Z\d@\.\+-_]{1,30}$'
        if not re.search(pattern, username):
            raise forms.ValidationError(_('Please, enter the valid user name. '
                                          'Username must be 30 or fewer '
                                          'characters. Use only English '
                                          'letters, numbers, and this '
                                          'symbols: @ . + - _'))
        if User.objects.filter(username=username)[0]:
            raise forms.ValidationError(_('User with this username is already '
                                          'exist'))

        return username

    def clean(self):
        data = self.cleaned_data
        if data.get('password') != data.get('confirm_password'):
            raise forms.ValidationError(_('Passwords must be same'))
        return data


class AuthorizationForm(forms.Form):
    username = forms.CharField(
        label=_('Username'),
        max_length=30,
        widget=forms.TextInput(
            attrs=dict_updater(attrs, auth_titles['username']),
        ),
    )
    password = forms.CharField(
        label=_('Password'),
        widget=forms.PasswordInput(
            attrs=dict_updater(attrs, auth_titles['password']),
        ),
    )

    def clean(self):
        cleaned_data = super(AuthorizationForm, self).clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        if not authenticate(username=username, password=password):
            raise forms.ValidationError(_('Please, enter valid username and '
                                          'password'))

        return cleaned_data


class UserEditForm(forms.Form):
    first_name = forms.CharField(
        label=_('First name'),
        max_length=30,
        widget=forms.TextInput(
            attrs=dict_updater(attrs, reg_titles['first_name']),
        ),
    )
    last_name = forms.CharField(
        label=_('Last name'),
        max_length=30,
        widget=forms.TextInput(
            attrs=dict_updater(attrs, reg_titles['last_name']),
        ),
    )


class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(
        required=False,
        label=_('Current password'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'data-toggle': 'tooltip',
                'data-placement': 'left',
                'title': _('Enter current password'),
            },
        )
    )
    new_password = forms.CharField(
        required=False,
        label=_('New password'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'data-toggle': 'tooltip',
                'data-placement': 'left',
                'title': _('Enter new password'),
            },
        )
    )
    confirm_password = forms.CharField(
        required=False,
        label=_('Confirmation password'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'data-toggle': 'tooltip',
                'data-placement': 'left',
                'title': reg_titles['confirm_password'],
            },
        )
    )

    def clean_current_password(self):
        password = self.cleaned_data['current_password']
        if password and not authenticate(username=self.request.user.usename,
                                         password=password):
            raise forms.ValidationError(_('Enter valid current password'))

        return password
    
    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()
        current_password = cleaned_data.get('current_password')
        new_password = cleaned_data.get('new_password')
        confirm_password = cleaned_data.get('confirm_password')

        self.is_empty = False
        if not current_password and not new_password and not confirm_password:
            self.is_empty = True
            return cleaned_data
        if not new_password or not confirm_password:
            raise forms.ValidationError(_('Password is empty'))
        if new_password != confirm_password:
            raise forms.ValidationError(_('Passwords must be same'))
        return cleaned_data
