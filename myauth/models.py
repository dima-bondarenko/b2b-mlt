import datetime

from django.contrib.auth.models import User
from django.db import models


class TempLink(models.Model):
    user = models.OneToOneField(User)
    link = models.SlugField(max_length=50)
    timeout = models.DateTimeField(default=datetime.date.today() +
                                           datetime.timedelta(2))
