from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class AuthAppHook(CMSApp):
    app_name = 'accounts'
    name = _('Authentication')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['myauth.urls']


apphook_pool.register(AuthAppHook)
