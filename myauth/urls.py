from django.conf.urls import url

from .views import confirm, login, logout, signup, user_edit

urlpatterns = [
    # url(r'^$', user_edit, name='user_edit'),
    url(r'^login$', login, name='login'),
    url(r'^signup$', signup, name='signup'),
    url(r'^logout$', logout, name='logout'),
    url(r'^(?P<link>[\dA-Fa-f]+)', confirm, name='confirm'),
]
