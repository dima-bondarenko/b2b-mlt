from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',
        views.CompaniesList.as_view(),
        name='list'),
    url(r'^created-by-user$', views.UserCompanies.as_view(),
        name='created-by-user'),
    url(r'^register$', views.CompanyRegister.as_view(), name='register'),
    url(r'^update/(?P<pk>\d+)/step-2$', views.company_data_update,
        name='step_2'),
    url(r'^update/(?P<pk>\d+)$', views.CompanyUpdate.as_view(), name='update'),
    url(r'^delete/(?P<pk>\d+)$', views.CompanyDelete.as_view(), name='delete'),
    url(r'^(?P<pk>\d+)$', views.CompanyDetail.as_view(), name='detail'),
    url(r'^(?P<category>[\w-]+)$', views.CompaniesList.as_view(),
        name='category'),
    url(r'^gallery/(\d+)$', views.get_gallery_src, name='gallery'),
    url(r'^gallery/all$', views.get_gallery_src, name='full_gallery'),
]
