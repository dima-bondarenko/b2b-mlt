import math

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views import generic
from django_ajax.decorators import ajax

from . import forms
from .models import Category, Company, Photo


class CompaniesList(generic.ListView):
    context_object_name = 'companies_list'

    def get_queryset(self):
        query = self.request.GET.get('search')
        category = self.kwargs.get('category')
        self.has_category_list = True
        if query is not None:
            # search
            self.has_category_list = False
            return Company.objects.filter(
                Q(name__icontains=query) |
                Q(overview__icontains=query) |
                Q(address__icontains=query) |
                Q(photo__product__icontains=query)
            ).distinct()
        elif category is None:
            # all companies
            return Company.objects.all()
        else:
            # companies by category
            category_obj = get_object_or_404(Category, slug=category)
            return Company.objects.filter(category=category_obj)

    def get_context_data(self, **kwargs):
        context = super(CompaniesList, self).get_context_data(**kwargs)
        context['active_category'] = self.kwargs.get('category')
        if self.has_category_list:
            context['categories'] = chunks(Category.objects.all(), 3)
        return context


def chunks(lst, chunk_count):
    chunk_size = int(math.ceil(len(lst) / chunk_count))
    return [lst[i:i + chunk_size] for i in range(0, len(lst), chunk_size)]


class UserCompanies(generic.ListView):
    context_object_name = 'companies_list'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserCompanies, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Company.objects.filter(created_by=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(UserCompanies, self).get_context_data(**kwargs)
        context['created_by_user'] = True
        return context


class CompanyDetail(generic.DetailView):
    model = Company
    context_object_name = 'company'

    def get_context_data(self, **kwargs):
        context = super(CompanyDetail, self).get_context_data(**kwargs)

        # Add to context verbose names of all Company fields.
        for field in Company._meta.fields:
            context[field.attname] = field.verbose_name

        return context


class CompanyRegister(generic.edit.CreateView):
    model = Company
    template_name = 'company/company_form_1.html'
    form_class = forms.CompanyForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CompanyRegister, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        company = form.save(commit=False)
        company.created_by = self.request.user
        company.save()
        company.category = form.cleaned_data.get('category')
        company.additionalInfo = form.cleaned_data.get('additionalInfo')
        company.save()

        return redirect(reverse_lazy('companies:step_2',
                                     kwargs={'pk': company.pk}))

    def get_context_data(self, **kwargs):
        context = super(CompanyRegister, self).get_context_data(**kwargs)
        context['submit_text'] = _('Register')
        return context


class CompanyUpdate(generic.edit.UpdateView):
    model = Company
    template_name = 'company/company_form_1.html'
    form_class = forms.CompanyForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        company = get_object_or_404(Company, pk=self.kwargs.get('pk'))
        if company.created_by != self.request.user:
            return redirect('/')
        else:
            return super(CompanyUpdate, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('companies:step_2',
                            kwargs={'pk':self.kwargs.get('pk')})

    def get_context_data(self, **kwargs):
        context = super(CompanyUpdate, self).get_context_data(**kwargs)
        context['submit_text'] = _('Next')
        return context


@login_required
def company_data_update(request, pk):
    try:
        company = Company.objects.get_all().get(pk=pk)
    except ObjectDoesNotExist:
        raise Http404()

    if not company.active:
        company.active = True
        company.save()

    if request.user != company.created_by:
        return redirect(reverse_lazy('companies:created-by-user'))

    if request.method == 'GET':
        phone_formset = forms.PhoneInline(instance=company, prefix='phone')
        email_formset = forms.EmailInline(instance=company, prefix='email')
        photo_formset = forms.PhotoInline(instance=company, prefix='photo')
        return render(
            request,
            'company/company_form_2.html',
            {
                'phone_formset': phone_formset,
                'email_formset': email_formset,
                'photo_formset': photo_formset,
                'submit_text': _('Update'),
            },
        )
    elif request.method == 'POST':
        formsets = (
            forms.PhoneInline(request.POST,
                              instance=company,
                              prefix='phone'),
            forms.EmailInline(request.POST,
                              instance=company,
                              prefix='email'),
            forms.PhotoInline(request.POST, request.FILES,
                              instance=company,
                              prefix='photo'),
        )
        if all(formset.is_valid() for formset in formsets):
            for formset in formsets:
                formset.instance = company
                formset.save()

            return redirect(reverse_lazy('companies:created-by-user'))
        else:
            return render(
                request,
                'company/company_form_2.html',
                {
                    'phone_formset': formsets[0],
                    'email_formset': formsets[1],
                    'photo_formset': formsets[2],
                    'submit_text': _('Update'),
                },
            )


class CompanyDelete(generic.edit.DeleteView):
    model = Company
    success_url = reverse_lazy('companies:created-by-user')


@ajax
def get_gallery_src(request, category_id=None):
    if category_id is None:
        gallery = Photo.objects.order_by('-pub_date')[:10]
    else:
        # This is one query.
        gallery = Photo.objects.filter(company__category__id=int(category_id))
        gallery = gallery.order_by('-pub_date')[:10]
    return render(request, 'company/gallery.html', {'gallery': gallery})
