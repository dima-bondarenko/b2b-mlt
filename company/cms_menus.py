from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import Category


class CategoryMenu(CMSAttachMenu):
    name = _('Categories menu')

    def get_nodes(self, request):
        nodes = []
        for category in Category.objects.all():
            node = NavigationNode(
                title=category.name,
                url=reverse('companies:category', args=[category.slug]),
                id=category.pk,
            )
            nodes.append(node)
        nodes.append(NavigationNode(title=_('All companies'),
                                    url=reverse('companies:list'),
                                    id='all-companies'))
        return nodes


menu_pool.register_menu(CategoryMenu)
