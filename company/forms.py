from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Company, Email, Phone, Photo

attrs = {
    'logo': {'class': 'form-control'},
    'name': {'class': 'form-control', 'required': 'required'},
    'foundation_year': {'class': 'form-control', 'required': 'required'},
    'category': {},
    'overview': {'class': 'form-control', 'required': 'required'},
    'USREOU': {'class': 'form-control', 'required': 'required'},
    'address': {'class': 'form-control', 'required': 'required'},
    'fax': {'class': 'form-control'},
    'web_site': {'class': 'form-control'},
    'additionalInfo': {},
}


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        exclude = ('active', 'created_by')
        localized_fields = '__all__'
        widgets = {
            'logo': forms.ClearableFileInput(attrs=attrs['logo']),
            'name': forms.TextInput(attrs=attrs['name']),
            'foundation_year': forms.NumberInput(
                attrs=attrs['foundation_year'],
            ),
            'category': forms.CheckboxSelectMultiple(attrs=attrs['category']),
            'overview': forms.Textarea(attrs=attrs['overview']),
            'USREOU': forms.NumberInput(attrs=attrs['USREOU']),
            'address': forms.TextInput(attrs=attrs['address']),
            'fax': forms.TextInput(attrs=attrs['fax']),
            'web_site': forms.URLInput(attrs=attrs['web_site']),
            'additionalInfo': forms.CheckboxSelectMultiple(
                attrs=attrs['additionalInfo'],
            ),
        }

    def clean_category(self):
        category = self.cleaned_data['category']
        if 1 > len(category) > 3:
            raise forms.ValidationError(_('Please, select from 1 to 3 '
                                          'categories'))

        return category

    def clean_USREOU(self):
        USREOU = self.cleaned_data['USREOU']
        if 7 > len(str(USREOU)) > 12:
            raise forms.ValidationError(
                _('USREOU code must be consists of minimum 7 digits, '
                  'a maximum of 12 digits'),
            )

        return USREOU


params = {
    'Phone': {
        'fields': ('phone',),
        'widgets': {
            'phone': forms.NumberInput(attrs={'class': 'form-control'}),
        },
    },
    'Email': {
        'fields': ('email',),
        'widgets': {
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }
    },
    'Photo': {
        'fields': ('product', 'image'),
        'widgets': {
            'product': forms.TextInput(attrs={'class': 'form-control'}),
            'image': forms.ClearableFileInput(attrs={'class': 'form-control'}),
        }
    },
}

PhoneInline = forms.inlineformset_factory(
    Company, Phone,
    **params['Phone'],
    extra=3,
    max_num=3, validate_max=True,
    min_num=1, validate_min=True,
)
EmailInline = forms.inlineformset_factory(
    Company, Email,
    **params['Email'],
    extra=3,
    max_num=3, validate_max=True,
    min_num=1, validate_min=True,
)
PhotoInline = forms.inlineformset_factory(
    Company, Photo,
    **params['Photo'],
    extra=10,
    max_num=10, validate_max=True,
)
