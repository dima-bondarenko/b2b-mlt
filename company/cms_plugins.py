from cms.models.pluginmodel import CMSPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from .models import Category
from .views import chunks


class CategoriesPlugin(CMSPluginBase):
    module = _('company plugins')
    name = _('categories list plugin')
    render_template = 'company/plugin.html'
    model = CMSPlugin

    def render(self, context, instance, placeholder):
        context.update({'instance': instance,
                        'categories': chunks(Category.objects.all(), 3)})
        return context


plugin_pool.register_plugin(CategoriesPlugin)
