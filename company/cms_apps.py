from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class CompaniesAppHook(CMSApp):
    app_name = 'companies'
    name = _('Companies application')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['company.urls']


apphook_pool.register(CompaniesAppHook)
