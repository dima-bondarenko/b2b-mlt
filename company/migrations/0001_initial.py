# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name='ukrainian name', max_length=50)),
                ('url', models.CharField(verbose_name='url', max_length=20)),
            ],
            options={
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(verbose_name="ім'я", max_length=50)),
                ('foundation_date', models.TimeField(verbose_name='foundation_date')),
                ('create_date', models.TimeField(verbose_name='create_date', auto_now_add=True)),
                ('update_date', models.TimeField(verbose_name='update_date', auto_now=True)),
                ('overview', models.TextField(verbose_name='overview', max_length=2000)),
                ('USREOU', models.IntegerField(verbose_name='USREOU')),
                ('address', models.CharField(verbose_name='address', max_length=20)),
                ('phone', models.IntegerField(verbose_name='phone')),
                ('fax', models.CharField(verbose_name='fax', max_length=20, blank=True)),
                ('e_mail', models.EmailField(verbose_name='e_mail', max_length=254)),
                ('web_site', models.URLField(verbose_name='web_site', blank=True)),
                ('agent', models.ManyToManyField(verbose_name='agent', blank=True, related_name='_company_agent_+', to='company.Company')),
                ('category', models.ManyToManyField(verbose_name='category', to='company.Category')),
                ('contractor', models.ManyToManyField(verbose_name='contractor', blank=True, related_name='_company_contractor_+', to='company.Company')),
                ('distributor', models.ManyToManyField(verbose_name='distributor', blank=True, related_name='_company_distributor_+', to='company.Company')),
                ('manufacturer', models.ManyToManyField(verbose_name='manufacturer', blank=True, related_name='_company_manufacturer_+', to='company.Company')),
                ('owner', models.ForeignKey(verbose_name='owner', to=settings.AUTH_USER_MODEL)),
                ('provider', models.ManyToManyField(verbose_name='provider', blank=True, related_name='_company_provider_+', to='company.Company')),
                ('retail', models.ManyToManyField(verbose_name='retail', blank=True, related_name='_company_retail_+', to='company.Company')),
                ('wholesale', models.ManyToManyField(verbose_name='wholesale', blank=True, related_name='_company_wholesale_+', to='company.Company')),
            ],
            options={
                'verbose_name': 'company',
                'verbose_name_plural': 'companies',
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('photo', models.ImageField(verbose_name='photo', upload_to='')),
                ('company', models.ForeignKey(verbose_name='company', to='company.Company')),
            ],
            options={
                'verbose_name': 'photo',
                'verbose_name_plural': 'photos',
            },
        ),
    ]
