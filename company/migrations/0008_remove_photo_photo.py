# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0007_auto_20170617_1632'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='photo',
        ),
    ]
