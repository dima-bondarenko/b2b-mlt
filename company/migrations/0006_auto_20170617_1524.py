# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0005_auto_20170617_1207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='USREOU',
            field=models.BigIntegerField(verbose_name='USREOU', unique=True, validators=[django.core.validators.RegexValidator(regex='^\\d{7,12}$', message='USREOU code consists of minimum 7 digits, a maximum of 12\n                    digits')]),
        ),
        migrations.AlterField(
            model_name='company',
            name='overview',
            field=models.TextField(verbose_name='overview', max_length=2000, help_text='Duration and experience, the description of the company,\n            products'),
        ),
    ]
