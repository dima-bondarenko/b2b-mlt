# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0027_auto_20170629_1706'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='product',
            field=models.CharField(max_length=30, verbose_name='product', help_text='Product on image. Please, select different products.'),
        ),
    ]
