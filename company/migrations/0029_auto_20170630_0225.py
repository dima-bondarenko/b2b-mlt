# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0028_auto_20170629_1721'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='active',
            field=models.BooleanField(verbose_name='активний', default=False, help_text='Company is active on the site?'),
        ),
        migrations.AlterField(
            model_name='photo',
            name='product',
            field=models.CharField(verbose_name='product', max_length=50, help_text='Product on image. Please, select different products.'),
        ),
    ]
