# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0014_auto_20170621_1303'),
    ]

    operations = [
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254, verbose_name='email')),
            ],
            options={
                'verbose_name_plural': 'emails',
                'verbose_name': 'email',
            },
        ),
        migrations.CreateModel(
            name='Phone',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('phone', models.IntegerField(verbose_name='phone')),
            ],
            options={
                'verbose_name_plural': 'phones',
                'verbose_name': 'phone',
            },
        ),
        migrations.RemoveField(
            model_name='company',
            name='e_mail',
        ),
        migrations.RemoveField(
            model_name='company',
            name='foundation_date',
        ),
        migrations.RemoveField(
            model_name='company',
            name='phone',
        ),
        migrations.AddField(
            model_name='company',
            name='foundation_year',
            field=models.PositiveSmallIntegerField(validators=[django.core.validators.MaxValueValidator(message="you can't register not yet existing company", limit_value=2017)], verbose_name='foundation year', blank=True, null=True, help_text='Year of Foundation'),
        ),
        migrations.AlterField(
            model_name='company',
            name='USREOU',
            field=models.BigIntegerField(validators=[django.core.validators.RegexValidator(message='USREOU code consists of minimum 7 digits, a maximum of 12\n                    digits', regex='^\\d{7,12}$')], help_text='USREOU', unique=True, verbose_name='USREOU'),
        ),
        migrations.AlterField(
            model_name='company',
            name='address',
            field=models.CharField(help_text='business address', max_length=20, verbose_name='address'),
        ),
        migrations.AlterField(
            model_name='company',
            name='category',
            field=models.ManyToManyField(help_text='Category of enterprises by types of products (no more than three)', verbose_name='category', to='company.Category'),
        ),
        migrations.AlterField(
            model_name='company',
            name='created_by',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, help_text='the uploader of the company', verbose_name='створено'),
        ),
        migrations.AlterField(
            model_name='company',
            name='fax',
            field=models.CharField(verbose_name='fax', max_length=20, blank=True, help_text='enterprise fax'),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(help_text='name of the company', max_length=50, verbose_name="ім'я", unique=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='web_site',
            field=models.URLField(verbose_name='web site', blank=True, help_text='company website'),
        ),
        migrations.AddField(
            model_name='phone',
            name='company',
            field=models.ForeignKey(verbose_name='company', to='company.Company'),
        ),
        migrations.AddField(
            model_name='email',
            name='company',
            field=models.ForeignKey(verbose_name='company', to='company.Company'),
        ),
    ]
