# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0011_auto_20170621_1251'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='agent',
        ),
        migrations.RemoveField(
            model_name='company',
            name='contractor',
        ),
        migrations.RemoveField(
            model_name='company',
            name='distributor',
        ),
        migrations.RemoveField(
            model_name='company',
            name='manufacturer',
        ),
        migrations.RemoveField(
            model_name='company',
            name='provider',
        ),
        migrations.RemoveField(
            model_name='company',
            name='retail',
        ),
        migrations.RemoveField(
            model_name='company',
            name='wholesale',
        ),
    ]
