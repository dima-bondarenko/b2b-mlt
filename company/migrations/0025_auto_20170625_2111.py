# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import company.models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0024_company_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.ImageField(verbose_name='logotype', upload_to=company.models.Company._generate_filename, help_text='Add company logotype'),
        ),
    ]
