# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0012_auto_20170621_1300'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='additionalInfo',
            field=models.ManyToManyField(verbose_name='additional information', to='company.AdditionalInfo', null=True, help_text='additional information about the company', blank=True),
        ),
    ]
