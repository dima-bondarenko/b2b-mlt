# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0008_remove_photo_photo'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='path',
            field=models.ImageField(verbose_name='image', blank=True, upload_to=''),
        ),
    ]
