# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0017_auto_20170621_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='category',
            field=models.ManyToManyField(to='company.Category', help_text='category of enterprises by types of products (no more than three)', verbose_name='category'),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(max_length=50, help_text='name of the company', unique=True, verbose_name='company name'),
        ),
        migrations.AlterField(
            model_name='company',
            name='overview',
            field=models.TextField(max_length=2000, help_text='duration and experience, the description of the company,\n            products', verbose_name='overview'),
        ),
    ]
