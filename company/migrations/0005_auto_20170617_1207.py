# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('company', '0004_auto_20170528_1820'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='created_by',
            field=models.ForeignKey(verbose_name='створено', blank=True, null=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='company',
            name='USREOU',
            field=models.IntegerField(verbose_name='USREOU', unique=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='e_mail',
            field=models.EmailField(verbose_name='email', max_length=254),
        ),
    ]
