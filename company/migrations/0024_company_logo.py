# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import company.models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0023_auto_20170625_1856'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='logo',
            field=models.ImageField(blank=True, upload_to=company.models.Company._generate_filename, null=True),
        ),
    ]
