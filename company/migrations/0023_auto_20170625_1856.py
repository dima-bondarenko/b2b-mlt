# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import company.models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0022_auto_20170625_1810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='image',
            field=models.ImageField(help_text='Sample products', upload_to=company.models.Photo._generate_filename, verbose_name='photo'),
        ),
    ]
