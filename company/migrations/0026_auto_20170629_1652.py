# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0025_auto_20170625_2111'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='product',
            field=models.CharField(max_length=20, verbose_name='product', blank=True, help_text='Product on image. Please, select different products.'),
        ),
        migrations.AddField(
            model_name='photo',
            name='pub_date',
            field=models.DateField(null=True, auto_now=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='USREOU',
            field=models.BigIntegerField(verbose_name='USREOU', unique=True, help_text='USREOU', validators=[django.core.validators.RegexValidator(regex='^\\d{7,12}$', message='USREOU code consists of minimum 7 digits, a maximum of 12 digits')]),
        ),
    ]
