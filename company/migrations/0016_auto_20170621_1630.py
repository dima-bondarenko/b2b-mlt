# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0015_auto_20170621_1601'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='additionalInfo',
            field=models.ManyToManyField(verbose_name='additional information', help_text='additional information about the company', to='company.AdditionalInfo', blank=True),
        ),
    ]
