# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0007_auto_20161016_1055'),
        ('company', '0009_photo_path'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='path',
        ),
        migrations.AddField(
            model_name='photo',
            name='image',
            field=filer.fields.image.FilerImageField(verbose_name='photo', blank=True, null=True, to='filer.Image'),
        ),
    ]
