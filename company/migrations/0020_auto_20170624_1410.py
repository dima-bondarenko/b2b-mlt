# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators
import filer.fields.image
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0019_producttype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='additionalinfo',
            name='info',
            field=models.CharField(help_text='Additional information about the company', verbose_name='additional information', max_length=30),
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(verbose_name='слаґ', max_length=20),
        ),
        migrations.AlterField(
            model_name='company',
            name='additionalInfo',
            field=models.ManyToManyField(blank=True, help_text='Additional information about the company', verbose_name='additional information', to='company.AdditionalInfo'),
        ),
        migrations.AlterField(
            model_name='company',
            name='address',
            field=models.CharField(help_text='Business address', verbose_name='address', max_length=20),
        ),
        migrations.AlterField(
            model_name='company',
            name='category',
            field=models.ManyToManyField(help_text='Category of enterprises by types of products (no more than three)', verbose_name='category', to='company.Category'),
        ),
        migrations.AlterField(
            model_name='company',
            name='created_by',
            field=models.ForeignKey(help_text='The uploader of the company', verbose_name='створено', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='company',
            name='fax',
            field=models.CharField(blank=True, help_text='Enterprise fax', verbose_name='fax', max_length=20),
        ),
        migrations.AlterField(
            model_name='company',
            name='foundation_year',
            field=models.PositiveSmallIntegerField(help_text='Year of Foundation', verbose_name='foundation year', validators=[django.core.validators.MaxValueValidator(message="You can't register not yet existing company", limit_value=2017)]),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(verbose_name='company name', help_text='Name of the company', unique=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='company',
            name='overview',
            field=models.TextField(help_text='Duration and experience, the description of the company,\n            products', verbose_name='overview', max_length=2000),
        ),
        migrations.AlterField(
            model_name='company',
            name='web_site',
            field=models.URLField(blank=True, help_text='Company website', verbose_name='web site'),
        ),
        migrations.AlterField(
            model_name='email',
            name='email',
            field=models.EmailField(help_text='email', verbose_name='email', max_length=254),
        ),
        migrations.AlterField(
            model_name='phone',
            name='phone',
            field=models.IntegerField(help_text='Телефон', verbose_name='phone'),
        ),
        migrations.AlterField(
            model_name='photo',
            name='image',
            field=filer.fields.image.FilerImageField(help_text='Sample products', verbose_name='photo', to='filer.Image'),
        ),
        migrations.AlterField(
            model_name='producttype',
            name='product_type',
            field=models.CharField(help_text='Type of products', verbose_name='product type', max_length=30),
        ),
    ]
