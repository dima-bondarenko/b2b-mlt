# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0013_company_additionalinfo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='additionalInfo',
            field=models.ManyToManyField(help_text='additional information about the company', to='company.AdditionalInfo', verbose_name='additional information'),
        ),
    ]
