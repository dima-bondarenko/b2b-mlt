# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0002_auto_20170526_1740'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='url',
            new_name='slug',
        ),
    ]
