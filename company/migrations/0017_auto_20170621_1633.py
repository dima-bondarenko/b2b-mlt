# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0016_auto_20170621_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='foundation_year',
            field=models.PositiveSmallIntegerField(help_text='Year of Foundation', verbose_name='foundation year', validators=[django.core.validators.MaxValueValidator(message="you can't register not yet existing company", limit_value=2017)]),
        ),
    ]
