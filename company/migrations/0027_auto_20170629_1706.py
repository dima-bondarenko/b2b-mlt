# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0026_auto_20170629_1652'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='producttype',
            name='company',
        ),
        migrations.DeleteModel(
            name='ProductType',
        ),
    ]
