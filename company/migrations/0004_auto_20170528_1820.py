# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0003_auto_20170526_1745'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='create_date',
        ),
        migrations.RemoveField(
            model_name='company',
            name='update_date',
        ),
        migrations.AlterField(
            model_name='company',
            name='phone',
            field=models.IntegerField(verbose_name='phone'),
        ),
    ]
