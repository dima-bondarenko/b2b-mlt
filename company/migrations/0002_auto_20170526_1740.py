# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='owner',
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(verbose_name="ім'я", max_length=50),
        ),
        migrations.AlterField(
            model_name='category',
            name='url',
            field=models.SlugField(verbose_name='slug', max_length=20),
        ),
        migrations.AlterField(
            model_name='company',
            name='create_date',
            field=models.DateField(verbose_name='create date', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='e_mail',
            field=models.EmailField(verbose_name='email', max_length=254, blank=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='foundation_date',
            field=models.DateField(verbose_name='foundation date'),
        ),
        migrations.AlterField(
            model_name='company',
            name='phone',
            field=models.IntegerField(verbose_name='phone', blank=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='update_date',
            field=models.DateField(verbose_name='update date', auto_now=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='web_site',
            field=models.URLField(verbose_name='web site', blank=True),
        ),
    ]
