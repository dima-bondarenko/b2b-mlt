# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0006_auto_20170617_1524'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='created_by',
            field=models.ForeignKey(verbose_name='створено', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(verbose_name="ім'я", max_length=50, unique=True),
        ),
    ]
