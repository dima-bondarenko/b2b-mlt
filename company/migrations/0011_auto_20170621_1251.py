# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0010_auto_20170618_1827'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalInfo',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('info', models.CharField(max_length=30, verbose_name='additional information', help_text='additional information about the company')),
            ],
            options={
                'verbose_name_plural': 'additional information about the company',
                'verbose_name': 'additional information about the company',
            },
        ),
        migrations.AlterField(
            model_name='photo',
            name='image',
            field=filer.fields.image.FilerImageField(to='filer.Image', verbose_name='photo'),
        ),
    ]
