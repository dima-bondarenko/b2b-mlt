# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0018_auto_20170622_2115'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_type', models.CharField(max_length=30, verbose_name='product type')),
                ('company', models.ForeignKey(to='company.Company', verbose_name='company')),
            ],
            options={
                'verbose_name': 'product type',
                'verbose_name_plural': 'product types',
            },
        ),
    ]
