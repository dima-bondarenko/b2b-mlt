from django.contrib import admin

from . import models


class PhotoInline(admin.TabularInline):
    model = models.Photo
    extra = 1


class PhoneInline(admin.StackedInline):
    model = models.Phone
    extra = 1


class EmailInline(admin.StackedInline):
    model = models.Email
    extra = 1


@admin.register(models.Company)
class CompanyAdmin(admin.ModelAdmin):
    exclude = ('active',)
    inlines = [PhoneInline, EmailInline, PhotoInline]


@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ['name', 'slug']


@admin.register(models.AdditionalInfo)
class AdditionalInfoAdmin(admin.ModelAdmin):
    pass
