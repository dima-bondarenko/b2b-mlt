from datetime import datetime
from random import random

from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Category(models.Model):
    name = models.CharField(_('name'), max_length=50)
    slug = models.SlugField(_('slug'), max_length=20)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')


class CompanyManager(models.Manager):
    def get_queryset(self):
        return super(CompanyManager, self).get_queryset().filter(active=True)

    def get_all(self):
        return super(CompanyManager, self).get_queryset()


class Company(models.Model):
    objects = CompanyManager()

    active = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('created by'),
        help_text=_('The uploader of the company'),
    )

    def _generate_filename(self, filename):
        return 'company_{}/logo/{}'.format(self.id, filename)

    logo = models.ImageField(
        upload_to=_generate_filename,
        verbose_name=_('logotype'),
        help_text=_('Add company logotype'),
    )
    name = models.CharField(
        _('company name'),
        max_length=50,
        unique=True,
        help_text=_('Name of the company'),
    )
    foundation_year = models.PositiveSmallIntegerField(
        _('foundation year'),
        validators=[
            MaxValueValidator(
                limit_value=datetime.now().year,
                message=_("You can't register not yet existing company"),
            ),
        ],
        help_text=_('Year of Foundation'),
    )
    category = models.ManyToManyField(
        Category,
        verbose_name=_('category'),
        help_text=_(
            'Category of enterprises by types of products (no more than three)',
        ),
    )

    overview = models.TextField(
        verbose_name=_('overview'),
        max_length=2000,
        help_text=_(
            '''Duration and experience, the description of the company,
            products'''
        ),
    )
    USREOU = models.BigIntegerField(
        verbose_name=_('USREOU'),
        unique=True,
        validators=[
            RegexValidator(
                regex=r'^\d{7,12}$',
                message=_('USREOU code consists of minimum 7 digits, '
                          'a maximum of 12 digits'),
            ),
        ],
        help_text=_('USREOU'),
    )
    address = models.CharField(
        _('address'),
        max_length=20,
        help_text=_('Business address'),
    )
    fax = models.CharField(
        _('fax'),
        max_length=20,
        blank=True,
        help_text=_('Enterprise fax'),
    )
    web_site = models.URLField(
        _('web site'),
        blank=True,
        help_text=_('Company website'),
    )
    additionalInfo = models.ManyToManyField(
        'AdditionalInfo',
        blank=True,
        verbose_name=_('additional information'),
        help_text=_('Additional information about the company'),
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse

        return reverse('companies:detail', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = _('company')
        verbose_name_plural = _('companies')


class Phone(models.Model):
    company = models.ForeignKey(Company,
                                on_delete=models.CASCADE,
                                verbose_name=_('company'))
    phone = models.IntegerField(_('phone'), help_text=_('Phone'))

    def __str__(self):
        return str(self.phone)

    class Meta:
        verbose_name = _('phone')
        verbose_name_plural = _('phones')


class Email(models.Model):
    company = models.ForeignKey(Company,
                                on_delete=models.CASCADE,
                                verbose_name=_('company'))
    email = models.EmailField(_('email'), help_text=_('email'))

    def __str__(self):
        return str(self.email)

    class Meta:
        verbose_name = _('email')
        verbose_name_plural = _('emails')


class Photo(models.Model):
    def _generate_filename(self, filename):
        dt = datetime.now().strftime('%d.%m.%Y %H-%M-%S')
        rnd = random()
        return 'company_{}/{}/{}/{}'.format(self.company.id, dt, rnd, filename)

    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        verbose_name=_('company'),
    )
    product = models.CharField(
        _('product'),
        max_length=50,
        help_text=_('Product on image. Please, select different products.'),
    )
    image = models.ImageField(
        upload_to=_generate_filename,
        verbose_name=_('photo'),
        help_text=_('Sample products'),
    )
    pub_date = models.DateField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return str(self.image)

    class Meta:
        verbose_name = _('photo')
        verbose_name_plural = _('photos')


class AdditionalInfo(models.Model):
    info = models.CharField(
        _('additional information'),
        max_length=30,
        help_text=_('Additional information about the company'),
    )

    def __str__(self):
        return self.info

    class Meta:
        verbose_name = _('additional information about the company')
        verbose_name_plural = _('additional information about the company')
