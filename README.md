# Комерческий портал г. Мелитополя #
## Как установить? ##
Запуск сервера для разработки
```
#!bash
pip3 install virtualenv
git clone https://dima-bondarenko@bitbucket.org/dima-bondarenko/b2b-mlt.git
cd b2b-mlt
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
python manage.py migrate         # После настройки БД.
python manage.py loaddata db.json
python manage.py runserver
```

wsgi.py находится в b2b/wsgi.py

Настройки — b2b/settings.py
