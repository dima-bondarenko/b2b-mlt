from django import forms
from django.utils.translation import ugettext_lazy as _
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget


class Callback(forms.Form):
    email = forms.EmailField(label='email', widget=forms.TextInput(attrs={'class': 'form-control'}))
    subject = forms.CharField(label=_('Subject'), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30)
    message = forms.CharField(label=_('Message'),
                              widget=forms.Textarea(attrs={'class': 'form-control'}),
                              max_length=2000)
    captcha = ReCaptchaField(widget=ReCaptchaWidget())
