from django.conf import settings
from django.core.mail import BadHeaderError, send_mail
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import Callback


def callback(request):
    template = 'callback/form.html'
    if request.method == 'GET':
        return render(request, template, {'form': Callback()})
    elif request.method == 'POST':
        form = Callback(request.POST)
        if form.is_valid():
            try:
                send_mail(
                    subject=form.cleaned_data.get('subject'),
                    message=form.cleaned_data.get('message'),
                    from_email=form.cleaned_data.get('email'),
                    recipient_list=[settings.EMAIL_HOST_USER],
                )
            except BadHeaderError:
                return render(request, template, {'form': form})
            return HttpResponseRedirect('/')
        else:
            return render(request, template, {'form': form})
