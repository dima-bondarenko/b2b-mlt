from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


class Callback(CMSApp):
    app_name = 'callback'
    name = _('Callback')

    def get_urls(self, page=None, language=None, **kwargs):
        return ['callback.urls']


apphook_pool.register(Callback)
